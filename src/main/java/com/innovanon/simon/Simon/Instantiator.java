/**
 * 
 */
package com.innovanon.simon.Simon;

/**
 * @author seanrdev
 *
 */
public interface Instantiator<T> {
	public T instantiate () ;
}
